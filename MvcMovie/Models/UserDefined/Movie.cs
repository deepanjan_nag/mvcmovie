﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcMovie.Models
{
    public class Movie
    {
        public int      ID          { get; set; }
        [StringLength(60, MinimumLength =3)]
        public string   Title       { get; set; }
        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        [Required]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [StringLength(30)]
        public string   Genre       { get; set; }
        [Range(1, 100)]
        [DataType(DataType.Currency)]
        public decimal  Price       { get; set; }
        [StringLength(5)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        public string   Rating      { get; set; }
    }
}